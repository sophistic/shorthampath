## Euclidean Shortest Hamiltonian Path Problem

Find the shortest path passing through all nodes. There is not constraint on the start and the end of the path.

### Context of the project

I've performed this exercise as a recruitment test. I worked on it for 2 days.

### Achievement

First I built the overall architecture: parser for the instances, framework to call different algorithms, methods to evaluate and display the results.

I started implementing simple heuristics based on the TSP litterature: Nearest Neighbour, Greedy, 2-Approximation.

The solutions returned by those algorithms could easily be improved by small changes. I implemented 2 routines:
 - a routine removing the intersections in the path,
 - a routine checking the solution path doesn't contain the longest edge from the induced cycle.

Then I used [elkai](https://github.com/fikisipi/elkai) (a Python library for solving the TSP based LKH by Keld Helsgaun) to develop more efficient algorithms solving the Shortest Hamiltonian Path Problem

### How to use it?

Most dependencies should be common libraries, except `elkai` and possibly `prettytable`.

The evaluation instances have not been uploaded to the git repository and need to be added in ``instances/evaluation``.

The algorithms can be tested from [main.py](https://gitlab.com/sophistic/shorthampath/-/blob/main/src/main.py) through a simple benchmarking framework to select the instances and the algorithms to run.

### Instance format

Instances are described by the coordinates of the nodes.
Distance between 2 nodes is given by the Euclidean distance.

```
NODE_COORD_SECTION
[index] [x_axis] [y_axis]
EOF
```

``index`` starts at 1.
