'''
Use of TSP best heuristic: Lin-Kernighan-Helsgaun

https://github.com/fikisipi/elkai

2 versions:
    - Rm edge: solve TSP on the initial instance then remove the longest edge 
    from solution cycle.
    - Dummy node: solve TSP on modified instance with an extra node which is
    at distance 0 from all nodes, then remove the node from the solution cycle
    to get the solution path.
'''
import elkai
import longestedge

def algo_rm_edge(dist_matrix):

    print("LKH rm edge")
    
    # create matrix of integer (values are multipled by 100)
    matrix = []
    for row in dist_matrix.values():
        matrix.append(list(map(lambda dist: int(dist*100), row.values())))
        
    tsp = [i+1 for i in elkai.solve_int_matrix(matrix)]
    
    return longestedge.remove_longest_edge(dist_matrix, tsp)

def algo_dummy_node(dist_matrix):

    print("LKH dummy node")
    
    # create matrix of integer (values are multipled by 100)
    # with additional dummy node (index 0) at distance 0 from other nodes
    matrix = [[0]*(len(dist_matrix)+1)]
    for row in dist_matrix.values():
        matrix.append([0] + list(map(lambda dist: int(dist*100), row.values())))
        
    tsp = elkai.solve_int_matrix(matrix)
    tsp.remove(0)
    
    return tsp

# instance4 = {1: {1: 0.0, 2: 10.0, 3: 10.0, 4: 14.14}, 2: {1: 10.0, 2: 0.0, 3: 14.14, 4: 10.0}, 3: {1: 10.0, 2: 14.14, 3: 0.0, 4: 10.0}, 4: {1: 14.14, 2: 10.0, 3: 10.0, 4: 0.0}}
# instance10 = {1: {1: 0.0, 2: 33.94, 3: 16.97, 4: 35.36, 5: 26.87, 6: 31.11, 7: 11.31, 8: 5.66, 9: 25.46, 10: 24.04}, 2: {1: 33.94, 2: 0.0, 3: 16.97, 4: 69.3, 5: 7.07, 6: 65.05, 7: 45.25, 8: 28.28, 9: 8.49, 10: 57.98}, 3: {1: 16.97, 2: 16.97, 3: 0.0, 4: 52.33, 5: 9.9, 6: 48.08, 7: 28.28, 8: 11.31, 9: 8.49, 10: 41.01}, 4: {1: 35.36, 2: 69.3, 3: 52.33, 4: 0.0, 5: 62.23, 6: 4.24, 7: 24.04, 8: 41.01, 9: 60.81, 10: 11.31}, 5: {1: 26.87, 2: 7.07, 3: 9.9, 4: 62.23, 5: 0.0, 6: 57.98, 7: 38.18, 8: 21.21, 9: 1.41, 10: 50.91}, 6: {1: 31.11, 2: 65.05, 3: 48.08, 4: 4.24, 5: 57.98, 6: 0.0, 7: 19.8, 8: 36.77, 9: 56.57, 10: 7.07}, 7: {1: 11.31, 2: 45.25, 3: 28.28, 4: 24.04, 5: 38.18, 6: 19.8, 7: 0.0, 8: 16.97, 9: 36.77, 10: 12.73}, 8: {1: 5.66, 2: 28.28, 3: 11.31, 4: 41.01, 5: 21.21, 6: 36.77, 7: 16.97, 8: 0.0, 9: 19.8, 10: 29.7}, 9: {1: 25.46, 2: 8.49, 3: 8.49, 4: 60.81, 5: 1.41, 6: 56.57, 7: 36.77, 8: 19.8, 9: 0.0, 10: 49.5}, 10: {1: 24.04, 2: 57.98, 3: 41.01, 4: 11.31, 5: 50.91, 6: 7.07, 7: 12.73, 8: 29.7, 9: 49.5, 10: 0.0}}
# print(algo_rm_edge(instance10))    
# print(algo_dummy_node(instance4))    

