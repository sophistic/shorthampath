'''
Shortest path inside the cycle.

A path is a cycle minus 1 edge. This method makes sure the "missing" edge in
the path is the longest from the cycle.
'''

def algo(dist_matrix, solution):
    
    dist_missing_edge = dist_matrix[solution[-1]][solution[0]]
    
    return remove_longest_edge(dist_matrix, solution, dist_missing_edge)

## Find and remove the longest edge from the tour if greater than max_dist
def remove_longest_edge(dist_matrix, tour, max_dist=0):
    
    longest_edge = ()
    longest_dist = max_dist
    
    for i in range(len(tour)-1):
        dist = dist_matrix[tour[i]][tour[i+1]]
        if dist > longest_dist:
            longest_dist = dist
            longest_edge = (i,i+1)
         
    if longest_edge:
        tour = tour[longest_edge[1]:] + tour[:longest_edge[1]]

    return tour    
        
# instance = {1: {1: 0.0, 2: 10.0, 3: 10.0, 4: 14.14}, 2: {1: 10.0, 2: 0.0, 3: 14.14, 4: 10.0}, 3: {1: 10.0, 2: 14.14, 3: 0.0, 4: 10.0}, 4: {1: 14.14, 2: 10.0, 3: 10.0, 4: 0.0}}
# solution = [1,4,2,3]
# print(algo(instance,solution))    