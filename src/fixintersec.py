'''
For a geometric instance, if there is an intersection inside the path, the 
solution can be improved by swapping the intersecting edges.

We compare 2 by 2 the edges of the solution path, if e_ij + e_kl > e_ik + e_jl,
there is an intersection and replacing (ij,kl) by (ik,jl) improves the solution.
'''

def algo(dist_matrix, solution):

    while(loop(dist_matrix, solution)>0.01):
        continue
    
    return solution

def loop(dist_matrix, solution):  
    
    gain = 0.0
    n = len(solution)
    
    for i in range(0,n-2):
        dist_ij = dist_matrix[solution[i]][solution[i+1]]
        
        # consecutive edges can't intersect
        for k in range(i+2,n-1):
            dist_kl = dist_matrix[solution[k]][solution[k+1]]
            
            dist_ik = dist_matrix[solution[i]][solution[k]]
            dist_jl = dist_matrix[solution[i+1]][solution[k+1]]
            
            diff = dist_ij + dist_kl - dist_ik - dist_jl
            if diff > 0.001: # prevent noise from computation accuracy 
                solution[i+1:k+1] = reversed(solution[i+1:k+1])
                gain += diff
                break
                
    return gain


# instance = {1: {1: 0.0, 2: 10.0, 3: 10.0, 4: 14.14}, 2: {1: 10.0, 2: 0.0, 3: 14.14, 4: 10.0}, 3: {1: 10.0, 2: 14.14, 3: 0.0, 4: 10.0}, 4: {1: 14.14, 2: 10.0, 3: 10.0, 4: 0.0}}
# solution = [1,4,2,3]
# print(algo(instance,solution))      