'''
Greedy list algorithm

Add shortest edges one by one while checking there is at most 2 edges per node 
and no cycle.
'''

def algo (edges_by_size, num_nodes):
    
    print("Greedy")
    
    # store each subpath by its endpoints
    solution = {}
    for i in range(1,num_nodes+1):
        solution[i] = [i]
    
    for value in sorted(edges_by_size):
        for i,j in edges_by_size[value]:
            
            if i in solution and j in solution and solution[i] is not solution[j]:
                
                # order and merge the 2 paths
                if solution[i][-1]!=i:
                    solution[i].reverse()
                if solution[j][0]!=j:
                    solution[j].reverse()
                merge = solution[i] + solution[j]
                
                # remove path from linked nodes
                del solution[i]
                del solution[j]
                
                # link new path to its endpoints
                solution[merge[-1]] = merge
                solution[merge[0]] = merge
                
                if len(merge) == num_nodes:
                    return merge

    return []
        
# edges={10.0: [[2, 1], [3, 1], [4, 2], [4, 3]], 14.14: [[3, 2], [4, 1]]}
# print(algo(edges, 4))