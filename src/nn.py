'''
Nearest Neighbour algorithm (greedy approach)

Random start: starting from a random node, build a path moving from node to
node by selecting the shortest edge at each node.

Best start: call the algorithm from each node and select the best solution

Returns the path solution as sequence of nodes
'''
from random import randrange
        
def algo_best_start (dist_matrix):
    
    print("NN best start")
    
    results = {}

    for node in dist_matrix.keys():
        path, length = algo(dist_matrix, node)
        results[length]= path
        
    shortest = min(results)
    return results[shortest]
          
def algo_random_start (dist_matrix):

    print("NN random start")
    
    solution, _ = algo(dist_matrix, randrange(1, len(dist_matrix) + 1))
    
    return solution
          
def algo (dist_matrix, starting_node):

    solution = {}
    path = []
    length = 0
    
    path.append(starting_node)
    
    curr_node = starting_node
    for _ in range(len(dist_matrix)-1):
        nn, dist = find_nearest_neighbour(dist_matrix, curr_node, solution)
        solution[curr_node]=nn
        path.append(nn)
        length += dist
        curr_node = nn
    
    return path, length
    
def find_nearest_neighbour(dist_matrix, curr, solution):
    
    def adjust_cost(item):
        # ignore current node and nodes already added to the solution
        if item[0] not in solution and item[0] != curr:
            return item[1]
        return float('inf')
        
    return min(dist_matrix[curr].items(), key=adjust_cost)
    
    
# data = {1: {1: 0.0, 2: 10.0, 3: 10.0, 4: 14.14}, 2: {1: 10.0, 2: 0.0, 3: 14.14, 4: 10.0}, 3: {1: 10.0, 2: 14.14, 3: 0.0, 4: 10.0}, 4: {1: 14.14, 2: 10.0, 3: 10.0, 4: 0.0}}
# # print(algo_random_start(data))
# print(algo_best_start(data))            