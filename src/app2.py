'''
Based on TSP 2-approximation algorithm using minimum spanning tree
'''
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree
from collections import defaultdict
import numpy as np

def algo (dist_matrix):
    
    print("2-approx")
    
    matrix = create_matrix(dist_matrix)
    mst = minimum_spanning_tree(matrix)
    edges = np.vstack(mst.nonzero()).T
    
    # index in mst starts at 0
    links = defaultdict(list)
    for edge in edges:
        links[edge[0]+1].append(edge[1]+1)
        links[edge[1]+1].append(edge[0]+1)

    # visit nodes of the spanning tree with a DFS
    path = []
    solution = {}
    # starts from a leaf of the spanning tree
    root = next(node for node,children in links.items() if len(children)==1)
    heap = [root]
    while heap:
        curr = heap.pop()
        heap.extend(filter(lambda node: node not in solution, links[curr]))
        path.append(curr)
        solution[curr] = curr
        
    return path
        
def create_matrix(dist_matrix):
    
    matrix = []
    
    for row in dist_matrix.values():
        matrix.append(list(row.values()))
    
    graph = csr_matrix(matrix)
   
    return graph
    
# instance = {1: {1: 0.0, 2: 10.0, 3: 10.0, 4: 14.14}, 2: {1: 10.0, 2: 0.0, 3: 14.14, 4: 10.0}, 3: {1: 10.0, 2: 14.14, 3: 0.0, 4: 10.0}, 4: {1: 14.14, 2: 10.0, 3: 10.0, 4: 0.0}}
# print(algo(instance))
