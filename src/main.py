'''
Main class calling the parser, the resolution and displaying the solution
'''

import time
import os
import matplotlib.pyplot as plt

import nn
import utils
import app2
import fixintersec
import longestedge
import greedy
import lkh

def solve (filename, option, improve=True, plot=False):
    
    data = init(filename, plot)
    
    start = time.time()
    if option == "NN rand":
        ## NN algorithm on 1 node
        solution = nn.algo_random_start(data.dist_matrix)
    elif option == "NN best":
        ## NN algorithm on all nodes
        solution = nn.algo_best_start(data.dist_matrix)
    elif option == "2-approx":
        ## 2-approximation
        solution = app2.algo(data.dist_matrix)
    elif option == "Greedy":
        ## Greedy algorithm
        solution = greedy.algo(data.edges_by_size,data.num_nodes)
    elif option == "LKH edge":
        ## LKH heuristic TSP
        solution = lkh.algo_rm_edge(data.dist_matrix)
    elif option == "LKH node":
        ## LKH heuristic SHPP
        solution = lkh.algo_dummy_node(data.dist_matrix)
    else:
        ## Default node order
        print("Default path")
        solution = list(range(1,data.num_nodes + 1))
        
    if improve:
        print("Intermediate solution length: %.2f" 
              % utils.compute_length(data.dist_matrix, solution))
        solution = fixintersec.algo(data.dist_matrix, solution)
        solution = longestedge.algo(data.dist_matrix, solution)
    
    end = time.time()
    runtime = end - start
    print("algo runtime: %.2f seconds" % runtime)
    
    length = print_solution(solution, plot, data, option, improve)
    
    return round(runtime,1), round(length,2)
    
def init(filename, plot):
    
    data = utils.parser(filename)
    if plot:
        utils.plot_instance(data.roadmap)
    utils.compute_all_distances(data)
    
    return data

def print_solution(solution, plot, data, option, improve):
    
    length = utils.compute_length(data.dist_matrix, solution)
    if plot:
        utils.plot_solution(solution,data, option, improve, length)
        output_dir = "output"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        plt.savefig(f'output/{data.name} - {option}.png')
    utils.print_solution(solution, length)
    
    return length
    
def print_benchmarking(res, scen, alg):
    
    from prettytable import PrettyTable
    
    t = PrettyTable([""] + [utils.pretty_name(instances[s]) for s in scen])
    
    for a in alg:
        t.add_row([algorithms[a]] + [format_res(r[a]) for r in res.values()])
    
    print(t)

def format_res(result):
    return str(result[1]) + ' (' + str(result[0]) + 's)'
    
    
## List of instances
instances = ["../instances/evaluation/51 nodes.txt",
             "../instances/evaluation/127 nodes.txt",
             "../instances/evaluation/280 nodes.txt",
             "../instances/test/4 nodes square.txt",
             "../instances/test/10 nodes line.txt",
             "../instances/test/25 nodes grid.txt"]

## List of algorithms
algorithms = ["NN rand", "NN best", "2-approx", "Greedy", "LKH edge", "LKH node"]

## Benchmarking
scenario = range(6)
algo = range(6)
imprve = True
plot = True

results={}

for s in scenario:
    results[s]={}
    for a in algo:
        results[s][a] = solve(instances[s], algorithms[a], imprve, plot)
        print("------------------------")
        
print_benchmarking (results, scenario, algo)
