'''
Contains utils methods for parsing, plotting and basic computing
'''

from collections import defaultdict
import matplotlib.pyplot as plt
import numpy as np

class instance:
    
    def __init__(self):
        
        # dict: node_index -> node_coordinate as a numpy array of int
        self.roadmap = {}
        
        # dict: node_index -> (dict: node_index -> edge_distance)
        self.dist_matrix = {}
        
        # defaultdict: edge_distance -> list(edge as a list of node_index)
        self.edges_by_size = defaultdict(list)
        
        self.name = ""
        self.num_nodes = 0

def parser (filename):
    
    data = instance();
    
    data.name = pretty_name(filename)
    
    print("Parsing %s" % filename)
    with open(filename, encoding='utf-8-sig') as file:
        
        for l in file:
            
            line = l.strip()
            
            if line == "NODE_COORD_SECTION":
                continue
            
            if line == "EOF":
                break
                
            i,x,y = line.split()
            data.roadmap[int(i)] = np.array([int(x),int(y)])
    
    data.num_nodes = len(data.roadmap)
    
    return data

def pretty_name (filename):
    return filename.split('/')[-1].split('.')[0]

def plot_instance (roadmap):
    
    plt.figure()
    for x,y in roadmap.values():
        plt.scatter(x, y)

def plot_solution (solution, data, algo, improve, length):
    
    plt.title(f'{data.name} - algo:{algo} / improve:{improve} - length: %.2f' % length)
    xlist, ylist = zip(*list(map(lambda i: data.roadmap[i], solution)))
    plt.plot(xlist, ylist)
    
def eucl_dist (x,y):
    return round(np.linalg.norm(x-y),2)
    
def compute_length (dist_matrix, solution):
    
    length = 0
    
    curr = solution[0]
    for i in solution[1:len(solution)]:
        dist = dist_matrix[curr][i]
        length += dist
        curr = i
        
    return length
    
def print_solution (solution, length):
    
    print("")
    print("length: %.2f" % length)
    print("path: ", " ".join(map(str,solution)))

def compute_all_distances (data):
    
    nodes = data.roadmap.items()
    for i,icoord in nodes:
        
        data.dist_matrix[i] = {}
        
        for j,jcoord in nodes:
            dist = eucl_dist(icoord, jcoord)
            data.dist_matrix[i][j] = dist
            # don't compute twice the same distance
            if j>=i:
                break
            data.dist_matrix[j][i] = dist
            data.edges_by_size[dist].append([i,j])

def find_any_leaf (tree):
    return next(node for node,children in tree.items() if len(children)==1)