# Report

## Algorithms

Here is the list of the different algorithms I implemented.

### Nearest Neighbour with Random Start (NN random)

Build incrementally the path moving from node to node by selecting the shortest available edge. 
Starting point is chosen randomly.

### Nearest Neighbour with Best Start (NN best)

Call the previous algorithm from each node of the instance and return the best solution.

### 2-Approximation Algorithm (2-Approx)

Well known 2-approximation for the Metric TSP based on the minimum spanning tree.
The path is then given by a Depth-First Search on the spanning tree.

### Shortest Edge List (Greedy)

List approach adding one by one the shortest edge to the solution while creating a path.

### Lin-Kernighan-Helsgaun with removed edge (LKH edge)

Lin-Kernighan-Helsgaun(LKH) is one of the best heuristics for solvinf the TSP.
It is a local search approach with adaptive neighbourhood.

LKH is used to find the shortest hamiltonian cycle.
Then the solution path is found by removing the longest edge from the cycle.

### Lin-Kernighan-Helsgaun with dummy node (LKH node)

LKH is used to find the shortest hamiltonian cycle in the instance modified with an additional dummy node equidistant to all other nodes (distance 0).
Then the solution path is found by removing the dummy node from the cycle.

### Improvements

After each algorithm's run, 2 post-processing routines perform easy improvements on the solution.
For the LKH algorithms, the routines have no impact but it can improve significantly the other algorithms.

**Remove intersections**

If 2 edges of the solution path intersect, because of the triangular inequality, permuting the edges improves the solution.

**Remove the longest edge of the cycle**

The solution path can be transformed in a cycle by adding an edge. 
If this missing edge is not the longest, the longest edge is removed to improve the solution.

## Results

### Overall

|          |    51 nodes   |    127 nodes     |    280 nodes    | 4 nodes square | 10 nodes line | 25 nodes grid |
|----------|---------------|------------------|-----------------|----------------|---------------|---------------|
| **NN rand**  | 445.44 (0.0s) | 118921.23 (0.0s) |  2744.09 (0.2s) |  30.0 (0.0s)   |  93.34 (0.0s) |  24.82 (0.0s) |
| **NN best**  | 428.37 (0.0s) | 114601.53 (0.4s) |  2732.5 (5.0s)  |  30.0 (0.0s)   |  69.29 (0.0s) |  24.82 (0.0s) |
| **2-approx** | 435.96 (0.0s) | 127435.91 (0.1s) |  2830.3 (0.4s)  |  30.0 (0.0s)   |  69.29 (0.0s) |  24.82 (0.0s) |
|  **Greedy**| 419.29 (0.0s) | 117844.08 (0.0s) |  2648.68 (0.1s) |  30.0 (0.0s)   |  69.29 (0.0s) |  24.41 (0.0s) |
| **LKH edge** | 414.93 (0.2s) | 109729.85 (1.6s) | 2565.12 (10.3s) |  30.0 (0.0s)   |  97.57 (0.0s) |  24.0 (0.0s)  |
| **LKH node** | 405.41 (0.2s) | 109042.29 (1.8s) | 2557.71 (10.4s) |  30.0 (0.0s)   |  69.29 (0.0s) |  24.0 (0.0s)  |

LKH algorithms give the best results but the runtime increases faster with the size of the instance.

Between the simpler heuristics, the Greedy algorithm seems to perform slightly better in average than the others.

### Best Solution on Evaluation Instances

`LKH node` gives the best solution for all instances.

**51 nodes**

```
length: 405.41
path:  40 42 19 41 13 25 14 24 43 7 23 48 6 27 51 46 12 47 18 4 17 37 44 15 45 33 39 10 30 34 21 29 2 16 50 9 49 5 38 11 32 1 22 8 26 31 28 3 20 35 36
```

![51_nodes_-_LKH_node](/uploads/70aca6036ca922e2bddae18c6b255524/51_nodes_-_LKH_node.png)

**127 nodes**

```
length: 109042.29
path:  107 111 112 94 46 118 48 53 49 47 55 66 113 65 99 92 89 125 104 110 85 86 87 88 109 96 119 63 102 101 83 82 126 81 84 117 78 76 75 69 70 71 68 74 73 67 8 72 19 22 4 23 24 9 11 3 90 116 60 59 62 61 91 58 64 100 10 115 13 120 7 105 114 6 106 15 108 20 17 21 18 77 79 80 27 31 12 14 41 30 26 25 33 29 32 122 28 38 39 42 34 43 40 35 36 37 16 1 2 51 50 5 52 124 56 121 57 54 44 45 103 93 127 95 123 97 98
```

![127_nodes_-_LKH_node](/uploads/b9166a70e33083864dfc2b13a06aac48/127_nodes_-_LKH_node.png)

**280 nodes**

```
length: 2557.71
path:  242 243 244 241 240 239 238 237 236 235 234 233 232 231 246 245 247 250 251 230 229 228 227 226 225 224 223 222 221 220 219 218 217 216 215 214 213 212 211 210 209 252 253 208 207 206 205 204 203 202 200 144 143 146 145 199 198 201 196 195 194 197 193 192 191 190 189 188 187 186 185 184 183 182 181 176 180 179 150 178 177 151 152 156 153 155 154 129 128 127 126 30 125 124 123 122 121 120 119 157 158 159 160 175 161 162 163 164 165 166 167 168 169 170 172 171 173 174 107 106 105 104 103 102 101 100 99 98 97 96 95 94 93 92 91 90 89 109 108 110 111 112 88 87 113 114 115 117 116 86 85 84 83 82 81 80 79 78 77 75 76 74 73 72 71 70 69 68 67 66 65 64 58 57 56 55 54 53 52 51 50 49 48 47 46 45 44 59 63 62 118 61 60 43 42 41 40 39 38 37 36 35 34 33 32 31 29 28 27 26 22 25 23 24 14 13 15 12 11 10 9 8 7 6 5 4 277 276 275 274 273 272 271 16 17 18 19 20 21 130 131 132 133 134 270 269 135 136 268 267 137 138 139 149 148 147 142 141 140 266 265 264 263 262 261 260 259 258 257 254 255 256 249 248 278 279 3 280 2 1
```

![280_nodes_-_LKH_node](/uploads/4e4f2601a961f247b0fde2884a7718b9/280_nodes_-_LKH_node.png)

